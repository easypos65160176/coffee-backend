export class CreateUserDto {
  email: string;
  fullname: string;
  password: string;
  gender: string;
  roles: string;
  image: string;
}
